﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using final.CAPA_LOGICA;


namespace final.CAPA_DATOS
{
    class ConsultasSQL
    {
        private SqlConnection conexion = new SqlConnection("Data Source = localhost; Initial Catalog = proyecto; Integrated Security = true");
        private DataSet ds;

        public DataTable MostrarDatos(string table)
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand(string.Format("select * from {0}", table), conexion);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataTable data = new DataTable();
            ad.Fill(data);
            conexion.Close();
            return data;
        }
        //lala, 123
        public DataTable consultaLogin(string nombre, string pass)
        {
            //abre la conexion con la base de datos
            conexion.Open();
            //me permite hacer las consultas a la base de datos
            SqlCommand cmd = new SqlCommand(string.Format("select * from usuarios where usuario = trim('{0}') and password = trim('{1}') and status = 1", nombre, pass), conexion);
            //ejecuta la consulta en la base de datos
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ds = new DataSet();
            ad.Fill(ds, "tabla");
            conexion.Close();

            return ds.Tables["tabla"];
        }

        public bool InsertarProductos(string fechaVen, string nombre, string precio, string contenido, string cantidad, string proveedor)
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand(string.Format("insert into productos(nombre,fecha_vencimiento,contenido,cantidad,precio,id_proveedor,status) values ('{0}', '{1}', '{2}', {3}, {4},  {5},1)", new string[] { nombre, fechaVen,contenido,cantidad,precio,proveedor }), conexion);
            int filasafectadas = cmd.ExecuteNonQuery();
            conexion.Close();
            if (filasafectadas > 0) {
                    return true;
                }
            else return false;
        }
        public DataTable ListarProductos()
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand(string.Format("select codigo, nombre, fecha_vencimiento,contenido, cantidad, precio,nombre_proveedor, productos.status  from productos join proveedores on proveedores.id = productos.id_proveedor"), conexion);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ds = new DataSet();
            ad.Fill(ds, "tabla");
            conexion.Close();
            return ds.Tables["tabla"];

        }
        public bool actualizarProducto(string fechaVen, string nombre, string precio, string contenido, string cantidad, string proveedor,string estado, string codigo)
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand(string.Format("UPDATE productos SET nombre = '{0}',fecha_vencimiento = '{1}',contenido = '{2}',cantidad = {3},precio ={4},id_proveedor = {5}, status = {6} where codigo = {7}", new string[] { nombre, fechaVen, contenido, cantidad, precio, proveedor,estado, codigo }), conexion);
            int filasafectadas = cmd.ExecuteNonQuery();
            conexion.Close();
            if (filasafectadas > 0)
            {
                return true;
            }
            else return false;
        }

        public bool InsertarProveedor(string nombre, string telefono, string direccion)
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand(string.Format("insert into proveedores(nombre_proveedor,telefono_proveedor,direccion_proveedor,status) values ('{0}', '{1}', '{2}', 1)", new string[] { nombre, telefono, direccion }), conexion);
            int filasafectadas = cmd.ExecuteNonQuery();
            conexion.Close();
            if (filasafectadas == 1)
            {
                return true;
            }
            else return false;
        }
        public DataTable listarProveedores()
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand(string.Format("select id,nombre_proveedor,telefono_proveedor,direccion_proveedor,status from proveedores"), conexion);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ds = new DataSet();
            ad.Fill(ds, "tabla");
            conexion.Close();
            return ds.Tables["tabla"];
        }

        public bool Eliminar(string id)
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand(string.Format("delete from Negocio where ID = {0}", id), conexion);
            int filasafectadas = cmd.ExecuteNonQuery();
            conexion.Close();
            if (filasafectadas > 0) return true;
            else return false;
        }

        public bool actualizarUsuario(string id, string nombre, string apellido, string usuario, string pass, string fechaNacimiento, string telefono, string id_rol, string intestado)
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand(string.Format("update usuarios set nombre = '{0}', apellido = '{1}', fecha_nacimiento = '{2}', telefono = {3}, usuario = trim('{4}'), password = trim('{5}'), id_rol = {6},status = {7} where id = {8}", new string[] { nombre, apellido, fechaNacimiento,telefono,usuario, pass,id_rol, intestado,id }), conexion);
            int filasafectadas = cmd.ExecuteNonQuery();
            conexion.Close();
            if (filasafectadas > 0) return true;
            else return false;
        }

        public DataTable consulta(string tabla, string id, string campo,string extra = "")
        {
            //ABRE CONEXION
            conexion.Open();
            //CREA LA CONSULTA
            SqlCommand cmd = new SqlCommand(string.Format("select * from {0} where {1} = '{2}' {3}", tabla, campo, id, extra), conexion);
            //EJECUTA CONSULTA
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            //GUARDA LOS DATOS EN UN DATASET QUE ES COMO UN ARRAY Y LOS DEVUELVE EN EL RETURN 
            ds = new DataSet();
            ad.Fill(ds, "tabla");
            conexion.Close();
            //AQUI LOS DEVUELVE
            return ds.Tables["tabla"];
        }
        public bool actualizarProveedor(string id, string nombre, string direccion, string telefono, string estado)
        {
            //ABRE CONEXION CON LA BD
            conexion.Open();
            //CREA LA CONSULTA
            SqlCommand cmd = new SqlCommand(string.Format("UPDATE proveedores SET nombre_proveedor = '{0}',telefono_proveedor = '{1}',direccion_proveedor = '{2}', status = {3} where id = {4}", new string[] { nombre, telefono, direccion, estado, id }), conexion);
            //EJECUNTA LA CONSULTA Y DEVUELVE EL NUMERO DE FILAS AFECTADAS, SI ES MAYOR  A CERO SI SE ACTUALIZO EL PROVEEDOR
            int filasafectadas = cmd.ExecuteNonQuery();
            //CIERRA LA CONEXION CON LA BD
            conexion.Close();
            //EN CASO DE QUE SE HAYAN AFECTADO FILAS ES QUE SI FUNCIONO LA CONSULTA
            if (filasafectadas == 1)
            {
                return true;
            }// EN CASO DE QUE NO SE RETORNA FALSO
            else return false;
        }

        public bool InsertarUsuario(string nombre, string apellido,string usuario,string pass,string telefono,string fechaNacimiento,string rol)
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand(string.Format("insert into usuarios(nombre,apellido,usuario,password,telefono,fecha_nacimiento,id_rol,status) values ('{0}', '{1}', trim('{2}'),'{3}',{4},'{5}','{6}',1)", new string[] { nombre, apellido, usuario, pass, telefono, fechaNacimiento, rol }), conexion);
            int filasafectadas = cmd.ExecuteNonQuery();
            conexion.Close();
            if (filasafectadas > 0)
            {
                return true;
            }
            else return false;
        }

        public DataTable ListarEmpleados()
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand(string.Format("select usuarios.id,usuarios.nombre,apellido,fecha_nacimiento,telefono,usuario, roles.nombre, usuarios.status from usuarios join roles on roles.id = usuarios.id_rol"), conexion);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ds = new DataSet();
            ad.Fill(ds, "tabla");
            conexion.Close();
            return ds.Tables["tabla"];
        }

        public string obtenerFunciones(string rol)
        {
            SqlCommand cmd = new SqlCommand(string.Format("select funcion from roles where id = {0}",rol), conexion);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataTable ds = new DataTable();
            ad.Fill(ds);
            string rolReturn = "";
            foreach (DataRow row in ds.Rows)
            {
                //TEXTO DEL ROL
                rolReturn = row["funcion"].ToString();
            }
            return rolReturn;
        }

        public bool InsertarRol(string nombre, string rol)
        {
            conexion.Open();
            ////Console.WriteLine(nombre+ "-"+rol);
            SqlCommand cmd = new SqlCommand(string.Format("insert into roles(nombre,funcion,status) values (trim('{0}'), '{1}',1)", new string[] { nombre, rol }), conexion);
            int filasafectadas = cmd.ExecuteNonQuery();
            conexion.Close();
            if (filasafectadas > 0)
            {
                return true;
            }
            else return false;
        }


        public bool actualizarRol(string id, string nombre, string funcion,string estado)
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand(string.Format("UPDATE roles SET nombre = trim('{0}'),funcion = '{1}', status = {2} where id = {3}", new string[] { nombre, funcion, estado, id }), conexion);
            int filasafectadas = cmd.ExecuteNonQuery();
            conexion.Close();
            if (filasafectadas > 0)
            {
                return true;
            }
            else return false;
        }

        public DataTable ListarRoles()
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand(string.Format("select id, nombre, funcion,status  from roles"), conexion);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ds = new DataSet();
            ad.Fill(ds, "tabla");
            conexion.Close();
            return ds.Tables["tabla"];

        }

        public bool insertarVenta(List<string> productos, string total)

        {
            conexion.Open();
            string usuario = Globales.usuarioid;
            SqlCommand cmd = new SqlCommand(string.Format("insert into ventas OUTPUT INSERTED.ID values(GETDATE(),{0})", usuario), conexion);
            Int32 idresultante = (Int32)cmd.ExecuteScalar();

            foreach(var id in productos)
            {
                string[] productocantidad ={ };
                productocantidad = id.Split(',');
                SqlCommand scmd = new SqlCommand(string.Format("insert into venta_productos values ({0},{1},{2})", new string[] { productocantidad[0], idresultante.ToString(), productocantidad[1] }), conexion);
                int filasafectadas = scmd.ExecuteNonQuery();
                SqlCommand scmdu = new SqlCommand(string.Format("update productos set cantidad =  cantidad - {0} where codigo = {1}", new string[] { productocantidad[1], productocantidad[0] }), conexion);
                scmdu.ExecuteNonQuery();
            }
            conexion.Close();
            if (idresultante > 0)
            {
                return true;
            }
            else return false;
            
        }

        public DataTable ListarProductosFactura()
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand(string.Format("select codigo,nombre, cantidad, precio from productos where status = 1 and cantidad >0"), conexion);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ds = new DataSet();
            ad.Fill(ds, "tabla");
            conexion.Close();
            return ds.Tables["tabla"];

        }

        public DataTable listarFacturas()
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand(string.Format("select v.id,fecha_venta,usuario from ventas v join usuarios u on u.id =  v.id_vendedor"), conexion);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ds = new DataSet();
            ad.Fill(ds, "tabla");
            conexion.Close();
            return ds.Tables["tabla"];

        }

        public DataTable ProductoxFactura(string id)
        {
            conexion.Open();
            SqlCommand cmd = new SqlCommand(string.Format("select nombre, precio, vt.cantidad from venta_productos vt join productos p on p.codigo = vt.codigo_producto where id_venta = {0}",id), conexion);
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ds = new DataSet();
            ad.Fill(ds, "tabla");
            conexion.Close();
            return ds.Tables["tabla"];

        }

    }
    
}
