﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using final.CAPA_DATOS;
using final.CAPA_LOGICA;

namespace CAPA_PRESENTACION
{
    public partial class ACTUALIZAR_ROL : Form
    {
       
        public ACTUALIZAR_ROL()
        {
            InitializeComponent();         
  
        }

        private void regreso_Click(object sender, EventArgs e)
        {
            this.Hide();
            ROLES pt = new ROLES();
            pt.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string id = textBoxBuscar.Text;
            string nombre = textBoxNombre.Text;
            string rolText = "";
            if (checkBoxFactura.Checked)
            {
                rolText = rolText + ",f";
            }
            if (checkBoxProductos.Checked)
            {
                rolText = rolText + ",p";
            }

            if (checkBoxProveedores.Checked)
            {
                rolText = rolText + ",pro";
            }

            if (checkBoxUsuarios.Checked)
            {
                rolText = rolText + ",e";
            }
            if (rolText.StartsWith(","))
            {

                rolText = rolText.Substring(1);
            }
            string estado = comboBoxEstado.SelectedIndex.ToString();
            string intestado = "";
            if (estado == "1")
            {
                intestado = "1";
            }
            else
                intestado = "0";
            ConsultasSQL sql = new ConsultasSQL();
            bool resultado = sql.actualizarRol(id,nombre, rolText,intestado);
            if (resultado)
            {
                MessageBox.Show("ROL ACTUALIZADO");
                this.Hide();
                ROLES pt = new ROLES();
                pt.ShowDialog();
            }
            else
                MessageBox.Show("OCURRIO UN ERROR, INTELO NUEVAMENTE");
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxProveedor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonBuscar_Click(object sender, EventArgs e)
        {
            ConsultasSQL sql = new ConsultasSQL();
            var isNumeric = int.TryParse(textBoxBuscar.Text, out int n);
            string numero = "";
            string extra = "";
            if (isNumeric)
            {
                numero = textBoxBuscar.Text;
            }
            else
            {

                numero = "0";
                extra = string.Format(" OR nombre = '{0}'", textBoxBuscar.Text);
            }
            if(textBoxBuscar.Text== "admin" || textBoxBuscar.Text =="1")
            {
                MessageBox.Show("ROL NO ENCONTRADO");
                return;
            }
            DataTable consulta = sql.consulta("roles", numero, "id", extra);
            checkBoxUsuarios.Checked = false;
            checkBoxFactura.Checked = false;
            checkBoxProductos.Checked = false;
            checkBoxProveedores.Checked = false;
            textBoxBuscar.Text = "";
            textBoxNombre.Text = "";
            if (consulta.Rows.Count > 0)
            {
                foreach (DataRow dataRow in consulta.Rows)
                {
                    
                    textBoxBuscar.Text = dataRow["id"].ToString();
                    textBoxNombre.Text = dataRow["nombre"].ToString().Trim();
                    Logica log = new Logica();
                    string[] funciones = log.obtenerFunciones(dataRow["id"].ToString());
                    if (funciones[0].ToString().Trim() != "all")
                    {
                        for (int i = 0; i < funciones.Length; i++)
                        {
                            switch (funciones[i].Trim())
                            {
                                case "e":
                                    checkBoxUsuarios.Checked = true;
                                    break;

                                case "f":
                                    checkBoxFactura.Checked = true;
                                    break;

                                case "p":
                                    checkBoxProductos.Checked = true;
                                    break;
                                case "pro":
                                    checkBoxProveedores.Checked = true;
                                    break;
                            }
                        }


                    }
                    string estado = "";
                    if (dataRow["status"].ToString() == "True")
                    {
                        estado = "ACTIVO";
                    }
                    else
                        estado = "INACTIVO";
                    comboBoxEstado.SelectedIndex = comboBoxEstado.FindStringExact(estado);
                }

            }
            else
            {
                MessageBox.Show("ROL NO ENCONTRADO");
            }



            


        }
    }
}
