﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using final.CAPA_DATOS;

namespace CAPA_PRESENTACION
{
    public partial class NUEVOROL : Form
    {
        private class Item
        {
            public string Name;
            public int Value;
            public Item(string name, int value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }
        public NUEVOROL()
        {
            InitializeComponent();
            ConsultasSQL sql = new ConsultasSQL();
            DataTable datos =  sql.MostrarDatos("proveedores");

           
           

        }

        private void regreso_Click(object sender, EventArgs e)
        {
            this.Hide();
            PRODUCTOS pt = new PRODUCTOS();
            pt.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string nombre = textBoxNombre.Text;
            string rolText = "";
            if (checkBoxFactura.Checked)
            {
                rolText = rolText + ",f";
            }
            if (checkBoxProductos.Checked)
            {
                rolText = rolText + ",p";
            }

            if (checkBoxProveedores.Checked)
            {
                rolText = rolText + ",pro";
            }

            if (checkBoxUsuarios.Checked)
            {
                rolText = rolText + ",e";
            }
            if (rolText.StartsWith(","))
            {

                rolText = rolText.Substring(1);
            }
            
            ConsultasSQL sql = new ConsultasSQL();
            bool resultado = sql.InsertarRol(nombre, rolText);
            if (resultado)
            {
                MessageBox.Show("ROL INSERTADO");
                this.Hide();
                PRODUCTOS pt = new PRODUCTOS();
                pt.ShowDialog();
            }else
                MessageBox.Show("OCURRIO UN ERROR, INTELO NUEVAMENTE");
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxProveedor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
