﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CAPA_PRESENTACION
{
    public partial class EMPLEADOS : Form
    {

        public EMPLEADOS()
        {
 
            InitializeComponent();
        }

        private void regreso_Click(object sender, EventArgs e)
        {
            this.Hide();
            menu_principal hr = new menu_principal();
            hr.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            agregar_empleado uuu = new agregar_empleado();
            uuu.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {this.Hide();
        ACTUALIZAR_EMPLEADOS nn = new ACTUALIZAR_EMPLEADOS();
        nn.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
            consultar_empleado iii = new consultar_empleado();
            iii.ShowDialog();
        }
    }
}
