﻿namespace CAPA_PRESENTACION
{
    partial class NUEVOROL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxNombre = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.regreso = new System.Windows.Forms.Button();
            this.checkBoxUsuarios = new System.Windows.Forms.CheckBox();
            this.checkBoxProductos = new System.Windows.Forms.CheckBox();
            this.checkBoxProveedores = new System.Windows.Forms.CheckBox();
            this.checkBoxFactura = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(268, 251);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 34;
            this.button1.Text = "GUARDAR";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxNombre
            // 
            this.textBoxNombre.Location = new System.Drawing.Point(210, 30);
            this.textBoxNombre.Name = "textBoxNombre";
            this.textBoxNombre.Size = new System.Drawing.Size(274, 20);
            this.textBoxNombre.TabIndex = 30;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(129, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "NOMBRE";
            // 
            // regreso
            // 
            this.regreso.BackgroundImage = global::final.CAPA_PRESENTACION.Properties.Resources.ikiki;
            this.regreso.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.regreso.Location = new System.Drawing.Point(12, 12);
            this.regreso.Name = "regreso";
            this.regreso.Size = new System.Drawing.Size(51, 38);
            this.regreso.TabIndex = 38;
            this.regreso.UseVisualStyleBackColor = true;
            this.regreso.Click += new System.EventHandler(this.regreso_Click);
            // 
            // checkBoxUsuarios
            // 
            this.checkBoxUsuarios.AccessibleDescription = "Usuarios";
            this.checkBoxUsuarios.AccessibleName = "Usuarios";
            this.checkBoxUsuarios.AutoCheck = false;
            this.checkBoxUsuarios.AutoSize = true;
            this.checkBoxUsuarios.Location = new System.Drawing.Point(210, 100);
            this.checkBoxUsuarios.Name = "checkBoxUsuarios";
            this.checkBoxUsuarios.Size = new System.Drawing.Size(67, 17);
            this.checkBoxUsuarios.TabIndex = 39;
            this.checkBoxUsuarios.Text = "Usuarios";
            this.checkBoxUsuarios.UseVisualStyleBackColor = true;
            // 
            // checkBoxProductos
            // 
            this.checkBoxProductos.AccessibleDescription = "Productos";
            this.checkBoxProductos.AccessibleName = "Productos";
            this.checkBoxProductos.AutoSize = true;
            this.checkBoxProductos.Location = new System.Drawing.Point(210, 146);
            this.checkBoxProductos.Name = "checkBoxProductos";
            this.checkBoxProductos.Size = new System.Drawing.Size(74, 17);
            this.checkBoxProductos.TabIndex = 40;
            this.checkBoxProductos.Text = "Productos";
            this.checkBoxProductos.UseVisualStyleBackColor = true;
            // 
            // checkBoxProveedores
            // 
            this.checkBoxProveedores.AccessibleDescription = "Proveedores";
            this.checkBoxProveedores.AccessibleName = "Proveedores";
            this.checkBoxProveedores.AutoSize = true;
            this.checkBoxProveedores.Location = new System.Drawing.Point(210, 123);
            this.checkBoxProveedores.Name = "checkBoxProveedores";
            this.checkBoxProveedores.Size = new System.Drawing.Size(86, 17);
            this.checkBoxProveedores.TabIndex = 41;
            this.checkBoxProveedores.Text = "Proveedores";
            this.checkBoxProveedores.UseVisualStyleBackColor = true;
            // 
            // checkBoxFactura
            // 
            this.checkBoxFactura.AccessibleDescription = "factura";
            this.checkBoxFactura.AccessibleName = "factura";
            this.checkBoxFactura.AutoSize = true;
            this.checkBoxFactura.Location = new System.Drawing.Point(210, 169);
            this.checkBoxFactura.Name = "checkBoxFactura";
            this.checkBoxFactura.Size = new System.Drawing.Size(62, 17);
            this.checkBoxFactura.TabIndex = 42;
            this.checkBoxFactura.Text = "Factura";
            this.checkBoxFactura.UseVisualStyleBackColor = true;
            // 
            // NUEVOROL
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::final.CAPA_PRESENTACION.Properties.Resources.hyhyhy;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(580, 373);
            this.Controls.Add(this.checkBoxFactura);
            this.Controls.Add(this.checkBoxProveedores);
            this.Controls.Add(this.checkBoxProductos);
            this.Controls.Add(this.checkBoxUsuarios);
            this.Controls.Add(this.regreso);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBoxNombre);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "NUEVOROL";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AGREGAR_PRODUCTOS";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxNombre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button regreso;
        private System.Windows.Forms.CheckBox checkBoxUsuarios;
        private System.Windows.Forms.CheckBox checkBoxProductos;
        private System.Windows.Forms.CheckBox checkBoxProveedores;
        private System.Windows.Forms.CheckBox checkBoxFactura;
    }
}