﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using final.CAPA_DATOS;

namespace CAPA_PRESENTACION
{
    public partial class AGREGAR_PRODUCTOS : Form
    {
        private class Item
        {
            public string Name;
            public int Value;
            public Item(string name, int value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }
        public AGREGAR_PRODUCTOS()
        {
            InitializeComponent();
            ConsultasSQL sql = new ConsultasSQL();
            DataTable datos =  sql.MostrarDatos("proveedores");

           
            foreach (DataRow dataRow in datos.Rows)
            {
                comboBoxProveedor.Items.Add(new Item(dataRow[1].ToString(), Int32.Parse(dataRow[0].ToString())));
            }

        }

        private void regreso_Click(object sender, EventArgs e)
        {
            this.Hide();
            PRODUCTOS pt = new PRODUCTOS();
            pt.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string nombre = textBoxNombre.Text;
            string fechaVencimiento = dateTimePicker1.Text;
            string contenido = textBoxContenido.Text;
            string cantidad = textBoxCantidad.Text;
            string precio = textBoxPrecio.Text;
            Item itm = (Item)comboBoxProveedor.SelectedItem;
            ConsultasSQL sql = new ConsultasSQL();
            bool resultado = sql.InsertarProductos(fechaVencimiento,nombre,precio,contenido,cantidad, itm.Value.ToString());
            if (resultado)
            {
                MessageBox.Show("PRODUCTO INSERTADO");
                this.Hide();
                PRODUCTOS pt = new PRODUCTOS();
                pt.ShowDialog();
            }else
                MessageBox.Show("OCURRIO UN ERROR, INTELO NUEVAMENTE");
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxProveedor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
