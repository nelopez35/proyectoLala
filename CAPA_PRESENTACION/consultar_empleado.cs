﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using final.CAPA_DATOS;

namespace CAPA_PRESENTACION
{
    public partial class consultar_empleado : Form
    {
        public consultar_empleado()
        {
            InitializeComponent();
            ConsultasSQL sql = new ConsultasSQL();
            DataTable datos = sql.ListarEmpleados();
            dataGridView1.DataSource = datos;
        }

        private void regreso_Click(object sender, EventArgs e)
        {
            this.Hide();
            EMPLEADOS NN = new EMPLEADOS();
            NN.ShowDialog();
        }
    }
}
