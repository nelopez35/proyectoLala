﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CAPA_PRESENTACION
{
    public partial class FACTURAS : Form
    {
        public FACTURAS()
        {
            InitializeComponent();
        }

        private void regreso_Click(object sender, EventArgs e)
        {
            this.Hide();
            menu_principal hr = new menu_principal();
            hr.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            CREAR_FACTURA yy = new CREAR_FACTURA();
            yy.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            CONSULTAR_FACTURAS oo = new CONSULTAR_FACTURAS();
            oo.ShowDialog();
        }

    }
}
