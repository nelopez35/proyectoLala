﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using final.CAPA_DATOS;

namespace CAPA_PRESENTACION
{
    public partial class actualizar_proveedores : Form
    {
        public actualizar_proveedores()
        {
            InitializeComponent();
        }

        private void regreso_Click(object sender, EventArgs e)
        {
            this.Hide();
            PROVEEDORES LL = new PROVEEDORES();
            LL.ShowDialog();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string id = textBoxBuscar.Text;
            string nombre = textBoxNombre.Text;
            string direccion = textBoxDireccion.Text;
            string telefono = textBoxTelefono.Text;
            string estado = comboBoxEstado.SelectedIndex.ToString();
            string intestado = "";
            if (estado == "1")
            {
                intestado = "1";
            }
            else
                intestado = "0";
            //llamar a las funciones del archivo ConsultasSQL
            ConsultasSQL sql = new ConsultasSQL();
            bool resultado = sql.actualizarProveedor(id, nombre, direccion, telefono, estado);
            if (resultado==true)
            {
                MessageBox.Show("PROVEEDOR ACTUALIZADO");
                this.Hide();
                PROVEEDORES pt = new PROVEEDORES();
                pt.ShowDialog();
            }
            else
                MessageBox.Show("OCURRIO UN ERROR, INTELO NUEVAMENTE");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConsultasSQL sql = new ConsultasSQL();
            //consulto el proveedor por el id que ingreso el usuario
            DataTable consulta = sql.consulta("proveedores", textBoxBuscar.Text, "id");
            //verifico si LA CONSULTA EJECUTADA ARRIBA TRAJO REGISTROS, SI ES MAYOR A CERO SIGNIFICA QUE SI
            if (consulta.Rows.Count > 0)
            {
                //ES NECESARIO ESTO PARA METER LOS DATOS TRAIDOS EN LA CONSULTA DE ARRIBA EN LOS INPUTS
                foreach (DataRow proveedor in consulta.Rows)
                {
                    //seteo los valores de los input de lo que me devuelve la consulta
                    textBoxBuscar.Text = proveedor[0].ToString();
                    textBoxNombre.Text = proveedor[1].ToString();
                    textBoxTelefono.Text = proveedor[2].ToString();
                    textBoxDireccion.Text = proveedor[3].ToString();
                    string estado = "";
                    Console.WriteLine(proveedor[4].ToString());
                    if (proveedor[4].ToString() == "True")
                    {
                        estado = "ACTIVO";
                    }
                    else
                        estado = "INACTIVO";
                    //ASIGNAR VALOR DE ACTIVO INACTIVO DEPENDIENDO DE LA BASE DE DATOS
                    comboBoxEstado.SelectedIndex = comboBoxEstado.FindStringExact(estado);
                }

            }
            else
                MessageBox.Show("NO SE ENCONTRO EL PROVEEDOR");
        }

        private void label2_Click(object sender, EventArgs e)
        {
           
        }
    }
}
