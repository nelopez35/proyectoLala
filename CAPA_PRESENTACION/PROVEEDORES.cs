﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CAPA_PRESENTACION
{
    public partial class PROVEEDORES : Form
    {

        public PROVEEDORES()
        {
            InitializeComponent();
        }

        private void PROVEEDORES_Load(object sender, EventArgs e)
        {

        }

        private void regreso_Click(object sender, EventArgs e)
        {
            this.Hide();
            menu_principal MN = new menu_principal();
            MN.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            agregar_proveedores uu = new agregar_proveedores();
            uu.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            actualizar_proveedores oo = new actualizar_proveedores();
            oo.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
            consultar_proveedores nn = new consultar_proveedores();
            nn.ShowDialog();
        }
    }
}
