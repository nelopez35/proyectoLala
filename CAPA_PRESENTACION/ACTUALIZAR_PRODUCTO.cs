﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using final.CAPA_DATOS;

namespace CAPA_PRESENTACION
{
    public partial class ACTUALIZAR_PRODUCTO : Form
    {
        private class Item
        {
            public string Name;
            public int Value;
            public Item(string name, int value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }
        public ACTUALIZAR_PRODUCTO()
        {
            InitializeComponent();
            ConsultasSQL sql = new ConsultasSQL();
            DataTable datos = sql.MostrarDatos("proveedores");


            foreach (DataRow dataRow in datos.Rows)
            {
                comboBoxProveedor.Items.Add(new Item(dataRow[1].ToString(), Int32.Parse(dataRow[0].ToString())));
            }
        }

        private void regreso_Click(object sender, EventArgs e)
        {
            this.Hide();
            PRODUCTOS YY = new PRODUCTOS();
            YY.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConsultasSQL sql = new ConsultasSQL();
            DataTable consulta = sql.consulta("productos",textBoxBuscar.Text,"codigo");
            if (consulta.Rows.Count > 0) {
                foreach (DataRow dataRow in consulta.Rows)
                {
                    textBoxBuscar.Text = dataRow[0].ToString();
                    textBoxCantidad.Text = dataRow[4].ToString();
                    textBoxContenido.Text = dataRow[3].ToString();
                    dateTimePicker1.Text = dataRow[2].ToString();
                    textBoxPrecio.Text = dataRow[5].ToString();
                    string estado = "";
                    Console.WriteLine(dataRow[7].ToString());
                    if (dataRow[7].ToString() =="True") {
                        estado = "ACTIVO";
                    }else
                        estado = "INACTIVO";
                    comboBoxEstado.SelectedIndex = comboBoxEstado.FindStringExact(estado);
                    //CONSULTO EL PROVEEDOR EN LA TABLA DE PROVEEDORES CON EL id_proveedor QUE ESTA ALMACENADO EN LA TABLA PRODUCTOS
                    DataTable proveedor = sql.consulta("proveedores", dataRow[6].ToString(), "id");
                    foreach (DataRow dataRowP in proveedor.Rows) {

                        comboBoxProveedor.SelectedIndex = comboBoxProveedor.FindStringExact(dataRowP[1].ToString());

                    }

                    
                    textBoxNombre.Text = dataRow[1].ToString();
                }
                
            }
            else
                MessageBox.Show("NO SE ENCONTRO EL PRODUCTO");

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string nombre = textBoxNombre.Text;
            string fechaVencimiento = dateTimePicker1.Text;
            string contenido = textBoxContenido.Text;
            string cantidad = textBoxCantidad.Text;
            string precio = textBoxPrecio.Text;
            string codigo = textBoxBuscar.Text;
            string estado = comboBoxEstado.SelectedIndex.ToString();
            string intestado = "";
            if (estado == "1")
            {
                intestado = "1";
            }
            else
                intestado = "0";
            Item itm = (Item)comboBoxProveedor.SelectedItem;
            ConsultasSQL sql = new ConsultasSQL();
            bool resultado = sql.actualizarProducto(fechaVencimiento, nombre, precio, contenido, cantidad, itm.Value.ToString(), intestado, codigo);
            if (resultado)
            {
                MessageBox.Show("PRODUCTO ACTUALIZADO");
                this.Hide();
                PRODUCTOS pt = new PRODUCTOS();
                pt.ShowDialog();
            }
            else
                MessageBox.Show("OCURRIO UN ERROR, INTELO NUEVAMENTE");
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
