﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using final.CAPA_DATOS;

namespace CAPA_PRESENTACION
{
    public partial class CONSULTAR_FACTURAS : Form
    {
        public CONSULTAR_FACTURAS()
        {
            InitializeComponent();
            ConsultasSQL sql = new ConsultasSQL();
            DataTable datos = sql.listarFacturas();
            dataGridView1.DataSource = datos;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.SelectionChanged += DataGridView_SelectionChanged;
        }

        private void DataGridView_SelectionChanged(object sender, EventArgs e)
        {
            var rowsCount = dataGridView1.SelectedRows;
            var productoseleccionado = dataGridView1.SelectedRows;
            string productosfactura = "";
            foreach (DataGridViewRow row in productoseleccionado)
            {
                string codigo = row.Cells["id"].Value.ToString();
                ConsultasSQL sql = new ConsultasSQL();
                DataTable productos = sql.ProductoxFactura(codigo);
                labelListadoProductos.Text = "";
                labelListadoProductos.Visible = false;
               
                foreach (DataRow producto in productos.Rows)
                {
                    int precio = Int32.Parse(producto["cantidad"].ToString()) * Int32.Parse(producto["precio"].ToString());
                    productosfactura += producto["nombre"].ToString() + 'X' + producto["cantidad"].ToString() + '=' + precio.ToString() + "\r\n";

                }
            }
            
            labelListadoProductos.Text = productosfactura;
            labelListadoProductos.Visible = true;
        }

        private void regreso_Click(object sender, EventArgs e)
        {
            this.Hide();
            FACTURAS PP = new FACTURAS();
            PP.ShowDialog();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void CONSULTAR_PRODUCTO_Load(object sender, EventArgs e)
        {

        }
    }
}
