﻿namespace CAPA_PRESENTACION
{
    partial class SPLASH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CARGANDO_SISTEMA = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.text_BIENVENIDO = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // CARGANDO_SISTEMA
            // 
            this.CARGANDO_SISTEMA.BackColor = System.Drawing.Color.Transparent;
            this.CARGANDO_SISTEMA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CARGANDO_SISTEMA.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CARGANDO_SISTEMA.Location = new System.Drawing.Point(257, 196);
            this.CARGANDO_SISTEMA.Name = "CARGANDO_SISTEMA";
            this.CARGANDO_SISTEMA.Size = new System.Drawing.Size(269, 21);
            this.CARGANDO_SISTEMA.TabIndex = 5;
            this.CARGANDO_SISTEMA.Text = "CARGANDO SISTEMA AL 0%";
            this.CARGANDO_SISTEMA.Click += new System.EventHandler(this.CARGANDO_SISTEMA_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.White;
            this.progressBar1.Location = new System.Drawing.Point(170, 160);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(429, 23);
            this.progressBar1.TabIndex = 7;
            this.progressBar1.Click += new System.EventHandler(this.progressBar1_Click);
            // 
            // text_BIENVENIDO
            // 
            this.text_BIENVENIDO.AutoSize = true;
            this.text_BIENVENIDO.BackColor = System.Drawing.Color.Transparent;
            this.text_BIENVENIDO.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_BIENVENIDO.Location = new System.Drawing.Point(307, 88);
            this.text_BIENVENIDO.Name = "text_BIENVENIDO";
            this.text_BIENVENIDO.Size = new System.Drawing.Size(161, 29);
            this.text_BIENVENIDO.TabIndex = 6;
            this.text_BIENVENIDO.Text = "BIENVENIDO";
            this.text_BIENVENIDO.Click += new System.EventHandler(this.text_BIENVENIDO_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // SPLASH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::final.CAPA_PRESENTACION.Properties.Resources.jhgaergxwertuioopikm;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(685, 301);
            this.Controls.Add(this.CARGANDO_SISTEMA);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.text_BIENVENIDO);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SPLASH";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "splash";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SPLASH_FormClosed);
            this.Load += new System.EventHandler(this.SPLASH_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CARGANDO_SISTEMA;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label text_BIENVENIDO;
        private System.Windows.Forms.Timer timer1;
    }
}