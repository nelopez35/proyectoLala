﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CAPA_PRESENTACION
{
    public partial class ROLES : Form
    {
        public ROLES()
        {
            InitializeComponent();
        }

        private void regreso_Click(object sender, EventArgs e)
        {
            this.Hide();
            menu_principal hr = new menu_principal();
            hr.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            NUEVOROL yy = new NUEVOROL();
            yy.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            CONSULTAR_ROLES oo = new CONSULTAR_ROLES();
            oo.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)       
        {
            this.Hide();
            ACTUALIZAR_ROL mm = new ACTUALIZAR_ROL();
            mm.ShowDialog();
        }
    }
}
