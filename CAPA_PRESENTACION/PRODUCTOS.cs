﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CAPA_PRESENTACION
{
    public partial class PRODUCTOS : Form
    {
        public PRODUCTOS()
        {
            InitializeComponent();
        }

        private void regreso_Click(object sender, EventArgs e)
        {
            this.Hide();
            menu_principal hr = new menu_principal();
            hr.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            AGREGAR_PRODUCTOS yy = new AGREGAR_PRODUCTOS();
            yy.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            CONSULTAR_PRODUCTO oo = new CONSULTAR_PRODUCTO();
            oo.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)       
        {
            this.Hide();
            ACTUALIZAR_PRODUCTO mm = new ACTUALIZAR_PRODUCTO();
            mm.ShowDialog();
        }
    }
}
