﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//me permite acceder a los archivos en la caperta CAPA LOGICA
using final.CAPA_LOGICA;

namespace CAPA_PRESENTACION
{
    public partial class menu_principal : Form
    {

        public menu_principal()
        {
            //SE ENCARGA DE MOSTRAR LOS BOTONES IMAGENES ETC DEL RECUADRO        
            InitializeComponent();
            //ID DEL ROL
            string rol = Globales.rol;
            Logica logica = new Logica();
            string[] funciones =  logica.obtenerFunciones(rol);
            //FUNCIONALIDAD QUE SE EJECUTA CUANDO EL USUARIO NO ES ADMINISTRADOR
            if (funciones[0].ToString().Trim() != "all")
            {
                //pongo invisibles todos los elementos del menu principal
                labelProductos.Visible = false;
                buttonProductos.Visible = false;
                buttonRoles.Visible = false;
                buttonEmpleados.Visible = false;
                labelEmpleados.Visible = false;
                buttonProveedores.Visible = false;
                labelProveedores.Visible = false;
                labelFactura.Visible = false;
                buttonFactura.Visible = false;
                //['e','pro','f']
                //  0   1     2
                for (int i = 0; i < funciones.Length; i++)
                {
                    switch (funciones[i].Trim())
                    {
                        case "e":
                            buttonEmpleados.Show();
                            labelEmpleados.Show();
                            break;

                        case "f":
                            labelFactura.Show();
                            buttonFactura.Show();
                            break;

                        case "p":
                            labelProductos.Show();
                            buttonProductos.Show();
                            break;
                        case "pro":
                            buttonProveedores.Show();
                            labelProveedores.Show();
                            break;
                    }
                }    
            }
            
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            LOGIN lo = new LOGIN();
            lo.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            PROVEEDORES b = new PROVEEDORES();
            b.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            PRODUCTOS l = new PRODUCTOS();
            l.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
           FACTURAS c = new FACTURAS();
            c.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            EMPLEADOS CC = new EMPLEADOS();
           CC.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            PROVEEDORES polook = new PROVEEDORES();
            polook.ShowDialog();
        }

        private void buttonRoles_Click(object sender, EventArgs e)
        {
            this.Hide();
            ROLES polook = new ROLES();
            polook.ShowDialog();
        }
    }
}
