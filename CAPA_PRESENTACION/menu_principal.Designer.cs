﻿namespace CAPA_PRESENTACION
{
    partial class menu_principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.labelFactura = new System.Windows.Forms.Label();
            this.labelProveedores = new System.Windows.Forms.Label();
            this.labelEmpleados = new System.Windows.Forms.Label();
            this.labelProductos = new System.Windows.Forms.Label();
            this.buttonFactura = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.buttonEmpleados = new System.Windows.Forms.Button();
            this.buttonProductos = new System.Windows.Forms.Button();
            this.buttonProveedores = new System.Windows.Forms.Button();
            this.buttonRoles = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(441, 219);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 16);
            this.label6.TabIndex = 24;
            this.label6.Text = "CERRAR";
            // 
            // button6
            // 
            this.button6.BackgroundImage = global::final.CAPA_PRESENTACION.Properties.Resources.CERAE;
            this.button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button6.Location = new System.Drawing.Point(444, 238);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(64, 64);
            this.button6.TabIndex = 23;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // labelFactura
            // 
            this.labelFactura.AutoSize = true;
            this.labelFactura.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFactura.Location = new System.Drawing.Point(59, 219);
            this.labelFactura.Name = "labelFactura";
            this.labelFactura.Size = new System.Drawing.Size(79, 16);
            this.labelFactura.TabIndex = 22;
            this.labelFactura.Text = "FACTURA";
            // 
            // labelProveedores
            // 
            this.labelProveedores.AutoSize = true;
            this.labelProveedores.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProveedores.Location = new System.Drawing.Point(389, 84);
            this.labelProveedores.Name = "labelProveedores";
            this.labelProveedores.Size = new System.Drawing.Size(123, 16);
            this.labelProveedores.TabIndex = 21;
            this.labelProveedores.Text = "PROVEEDORES";
            // 
            // labelEmpleados
            // 
            this.labelEmpleados.AutoSize = true;
            this.labelEmpleados.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEmpleados.Location = new System.Drawing.Point(233, 219);
            this.labelEmpleados.Name = "labelEmpleados";
            this.labelEmpleados.Size = new System.Drawing.Size(100, 16);
            this.labelEmpleados.TabIndex = 20;
            this.labelEmpleados.Text = "EMPLEADOS";
            // 
            // labelProductos
            // 
            this.labelProductos.AutoSize = true;
            this.labelProductos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProductos.Location = new System.Drawing.Point(129, 84);
            this.labelProductos.Name = "labelProductos";
            this.labelProductos.Size = new System.Drawing.Size(103, 16);
            this.labelProductos.TabIndex = 19;
            this.labelProductos.Text = "PRODUCTOS";
            this.labelProductos.Click += new System.EventHandler(this.label2_Click);
            // 
            // buttonFactura
            // 
            this.buttonFactura.BackgroundImage = global::final.CAPA_PRESENTACION.Properties.Resources.informes;
            this.buttonFactura.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonFactura.Location = new System.Drawing.Point(75, 238);
            this.buttonFactura.Name = "buttonFactura";
            this.buttonFactura.Size = new System.Drawing.Size(64, 64);
            this.buttonFactura.TabIndex = 17;
            this.buttonFactura.UseVisualStyleBackColor = true;
            this.buttonFactura.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(922, 133);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(97, 400);
            this.button4.TabIndex = 16;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // buttonEmpleados
            // 
            this.buttonEmpleados.BackColor = System.Drawing.Color.White;
            this.buttonEmpleados.BackgroundImage = global::final.CAPA_PRESENTACION.Properties.Resources.HBVTUN1;
            this.buttonEmpleados.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonEmpleados.Location = new System.Drawing.Point(259, 238);
            this.buttonEmpleados.Name = "buttonEmpleados";
            this.buttonEmpleados.Size = new System.Drawing.Size(65, 64);
            this.buttonEmpleados.TabIndex = 15;
            this.buttonEmpleados.Text = " ";
            this.buttonEmpleados.UseVisualStyleBackColor = false;
            this.buttonEmpleados.Click += new System.EventHandler(this.button3_Click);
            // 
            // buttonProductos
            // 
            this.buttonProductos.BackgroundImage = global::final.CAPA_PRESENTACION.Properties.Resources.nyqpl;
            this.buttonProductos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonProductos.Location = new System.Drawing.Point(145, 121);
            this.buttonProductos.Name = "buttonProductos";
            this.buttonProductos.Size = new System.Drawing.Size(64, 64);
            this.buttonProductos.TabIndex = 14;
            this.buttonProductos.UseVisualStyleBackColor = true;
            this.buttonProductos.Click += new System.EventHandler(this.button2_Click);
            // 
            // buttonProveedores
            // 
            this.buttonProveedores.BackColor = System.Drawing.Color.White;
            this.buttonProveedores.BackgroundImage = global::final.CAPA_PRESENTACION.Properties.Resources.PROBNDIHPROVCLAIBC;
            this.buttonProveedores.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonProveedores.Location = new System.Drawing.Point(418, 121);
            this.buttonProveedores.Name = "buttonProveedores";
            this.buttonProveedores.Size = new System.Drawing.Size(64, 64);
            this.buttonProveedores.TabIndex = 25;
            this.buttonProveedores.UseVisualStyleBackColor = false;
            this.buttonProveedores.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonRoles
            // 
            this.buttonRoles.BackColor = System.Drawing.SystemColors.Highlight;
            this.buttonRoles.Location = new System.Drawing.Point(535, 12);
            this.buttonRoles.Name = "buttonRoles";
            this.buttonRoles.Size = new System.Drawing.Size(75, 23);
            this.buttonRoles.TabIndex = 26;
            this.buttonRoles.Text = "roles";
            this.buttonRoles.UseVisualStyleBackColor = false;
            this.buttonRoles.Click += new System.EventHandler(this.buttonRoles_Click);
            // 
            // menu_principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::final.CAPA_PRESENTACION.Properties.Resources.oi;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(622, 393);
            this.Controls.Add(this.buttonRoles);
            this.Controls.Add(this.buttonProveedores);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.labelFactura);
            this.Controls.Add(this.labelProveedores);
            this.Controls.Add(this.labelEmpleados);
            this.Controls.Add(this.labelProductos);
            this.Controls.Add(this.buttonFactura);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.buttonEmpleados);
            this.Controls.Add(this.buttonProductos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "menu_principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "menu_principal";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label labelFactura;
        private System.Windows.Forms.Label labelProveedores;
        private System.Windows.Forms.Label labelEmpleados;
        private System.Windows.Forms.Label labelProductos;
        private System.Windows.Forms.Button buttonFactura;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button buttonEmpleados;
        private System.Windows.Forms.Button buttonProductos;
        private System.Windows.Forms.Button buttonProveedores;
        private System.Windows.Forms.Button buttonRoles;
    }
}