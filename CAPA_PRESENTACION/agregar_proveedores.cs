﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using final.CAPA_DATOS;

namespace CAPA_PRESENTACION
{
    public partial class agregar_proveedores : Form
    {
        public agregar_proveedores()
        {
            InitializeComponent();
        }

        private void regreso_Click(object sender, EventArgs e)
        {
            this.Hide();
            PROVEEDORES VV = new PROVEEDORES();
            VV.ShowDialog(); 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string nombre = textBoxNombre.Text;
            string telefono = textBoxTelefono.Text;
            string direccion = textBoxDireccion.Text;
           
            ConsultasSQL sql = new ConsultasSQL();
            bool resultado = sql.InsertarProveedor(nombre,telefono,direccion);
            //valido si se ingreso el registro en la base de datos
            if (resultado==true)
            {
                MessageBox.Show("PROVEEDOR INSERTADO");
                this.Hide();
                PROVEEDORES pt = new PROVEEDORES();
                pt.ShowDialog();
            }
            else
                MessageBox.Show("OCURRIO UN ERROR, INTELO NUEVAMENTE");
        }
    }
}
