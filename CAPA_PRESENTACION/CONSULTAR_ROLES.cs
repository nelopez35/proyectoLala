﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using final.CAPA_DATOS;

namespace CAPA_PRESENTACION
{
    public partial class CONSULTAR_ROLES : Form
    {
        public CONSULTAR_ROLES()
        {
            InitializeComponent();
            ConsultasSQL sql = new ConsultasSQL();
            DataTable datos = sql.ListarRoles();
            dataGridView1.DataSource = datos;
        }

        private void regreso_Click(object sender, EventArgs e)
        {
            this.Hide();
            ROLES PP = new ROLES();
            PP.ShowDialog();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void CONSULTAR_PRODUCTO_Load(object sender, EventArgs e)
        {

        }
    }
}
