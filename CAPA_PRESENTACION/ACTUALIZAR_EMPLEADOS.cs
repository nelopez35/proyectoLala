﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using final.CAPA_DATOS;

namespace CAPA_PRESENTACION
{
    public partial class ACTUALIZAR_EMPLEADOS : Form
    {
        private class Item
        {
            public string Name;
            public int Value;
            public Item(string name, int value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }
        public ACTUALIZAR_EMPLEADOS()
        {
            InitializeComponent();
            ConsultasSQL sql = new ConsultasSQL();
            DataTable datos = sql.MostrarDatos("roles");


            foreach (DataRow dataRow in datos.Rows)
            {
                comboBoxRol.Items.Add(new Item(dataRow[1].ToString(), Int32.Parse(dataRow[0].ToString())));
            }
        }

        private void regreso_Click(object sender, EventArgs e)
        {
            this.Hide();
            EMPLEADOS BR = new EMPLEADOS();
            BR.ShowDialog();
        }

        private void ACTUALIZAR_EMPLEADOS_Load(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConsultasSQL sql = new ConsultasSQL();
            var isNumeric = int.TryParse(textBoxBuscar.Text, out int n);
            string numero = "";
            string extra = "";
            if (isNumeric)
            {
                numero = textBoxBuscar.Text;
            }
            else {

                numero = "0";
                extra = string.Format(" OR usuario = '{0}'", textBoxBuscar.Text);
            }
                
            
            DataTable consulta = sql.consulta("usuarios", numero, "id",extra);
            if (consulta.Rows.Count > 0)
            {
                foreach (DataRow dataRow in consulta.Rows)
                {

                    textBoxBuscar.Text = dataRow[0].ToString();
                    textBoxNombre.Text = dataRow[1].ToString();
                    textBoxApellido.Text = dataRow[2].ToString();
                    textBoxUsuario.Text = dataRow[5].ToString();
                    textBoxContra.Text = dataRow[6].ToString();
                    dateTimePicker1.Text = dataRow[3].ToString();
                    textBoxTelefono.Text = dataRow[4].ToString();
                    string estado = "";
                    Console.WriteLine(dataRow[8].ToString());
                    if (dataRow[8].ToString() == "True")
                    {
                        estado = "ACTIVO";
                    }
                    else
                        estado = "INACTIVO";
                    comboBoxEstado.SelectedIndex = comboBoxEstado.FindStringExact(estado);
                    DataTable proveedor = sql.consulta("roles", dataRow[7].ToString(), "id");
                    foreach (DataRow dataRowP in proveedor.Rows)
                    {

                        comboBoxRol.SelectedIndex = comboBoxRol.FindStringExact(dataRowP[1].ToString());

                    }


                    textBoxNombre.Text = dataRow[1].ToString();
                }

            }
            else
                MessageBox.Show("NO SE ENCONTRO EL EMPLEADO");
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string id = textBoxBuscar.Text;
            string nombre = textBoxNombre.Text;
            string apellido = textBoxApellido.Text;
            string usuario = textBoxUsuario.Text;
            string pass = textBoxContra.Text;
            string fechaNacimiento = dateTimePicker1.Text;
            string telefono = textBoxTelefono.Text;
            string estado = comboBoxEstado.SelectedIndex.ToString();
            string intestado = "";
            if (estado == "1")
            {
                intestado = "1";
            }
            else
                intestado = "0";
            Item itm = (Item)comboBoxRol.SelectedItem;
            ConsultasSQL sql = new ConsultasSQL();
            bool resultado = sql.actualizarUsuario(id,nombre,apellido,usuario,pass,fechaNacimiento,telefono, itm.Value.ToString(),intestado);
            if (resultado)
            {
                MessageBox.Show("USUARIO ACTUALIZADO");
                this.Hide();
                EMPLEADOS pt = new EMPLEADOS();
                pt.ShowDialog();
            }
            else
                MessageBox.Show("OCURRIO UN ERROR, INTELO NUEVAMENTE");
        }
    }
}
