﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using final.CAPA_DATOS;

namespace CAPA_PRESENTACION
{
    public partial class agregar_empleado : Form
    {
        private class Item
        {
            public string Name;
            public int Value;
            public Item(string name, int value)
            {
                Name = name; Value = value;
            }
            public override string ToString()
            {
                // Generates the text shown in the combo box
                return Name;
            }
        }
        public agregar_empleado()
        {
            InitializeComponent();
            ConsultasSQL sql = new ConsultasSQL();
            DataTable datos = sql.MostrarDatos("roles");


            foreach (DataRow dataRow in datos.Rows)
            {
                comboBox1.Items.Add(new Item(dataRow[1].ToString(), Int32.Parse(dataRow[0].ToString())));
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void regreso_Click(object sender, EventArgs e)  
        {
            this.Hide();
            EMPLEADOS XX = new EMPLEADOS();
            XX.ShowDialog();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string nombre = textBoxNombre.Text;
            string apellido = textBoxApellido.Text;
            string usuario = textBoxUsuario.Text;
            string pass = textBoxContra.Text;
            string telefono = textBoxTelefono.Text;
            string fechaNacimiento = dateTimePicker1.Text;
            //obtiene el id del rol seleccionado
            Item itm = (Item)comboBox1.SelectedItem;
            string id_rol = itm.Value.ToString();
            ConsultasSQL sql = new ConsultasSQL();
            bool resultado = sql.InsertarUsuario(nombre, apellido, usuario, pass, telefono, fechaNacimiento, id_rol);
            if (resultado)
            {
                MessageBox.Show("USUARIO CREADO");
                this.Hide();
                EMPLEADOS pt = new EMPLEADOS();
                pt.ShowDialog();
            }
            else
                MessageBox.Show("OCURRIO UN ERROR, INTELO NUEVAMENTE");
        }
    }
}
