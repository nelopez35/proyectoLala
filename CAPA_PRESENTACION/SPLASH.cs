﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using final.CAPA_LOGICA;

namespace CAPA_PRESENTACION
{
    public partial class SPLASH : Form
    {
        //el rol y el usuario vienen del login
        public SPLASH(string rol,string usuario)
        {
            //GUARDAR EL ROL Y EL ID DEL USUARIO DURANTE EL TIEMPO QUE EL PROGRAMA ESTA ABIERTO
            Globales.rol = rol;
            Globales.usuarioid = usuario;
            InitializeComponent();
        }

        private void CARGANDO_SISTEMA_Click(object sender, EventArgs e)
        {

        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void text_BIENVENIDO_Click(object sender, EventArgs e)
        {

        }

        private void SPLASH_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (progressBar1.Value < 100)
            {
                progressBar1.Value = progressBar1.Value + 20;
                CARGANDO_SISTEMA.Text = "CARGANDO SISTEMA AL %" + progressBar1.Value + "&";
            }
            else { 
            
                timer1.Enabled = false;
                MessageBox.Show("CARGA COMPLETA");
                this.Close();
            }
        }

        private void SPLASH_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            menu_principal p = new menu_principal();
            p.ShowDialog(); 
        }
    }
}
