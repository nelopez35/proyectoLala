﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using final.CAPA_DATOS;

namespace CAPA_PRESENTACION
{
    public partial class CONSULTAR_PRODUCTO : Form
    {
        public CONSULTAR_PRODUCTO()
        {
            InitializeComponent();
            ConsultasSQL sql = new ConsultasSQL();
            DataTable datos = sql.ListarProductos();
            dataGridView1.DataSource = datos;
        }

        private void regreso_Click(object sender, EventArgs e)
        {
            this.Hide();
            PRODUCTOS PP = new PRODUCTOS();
            PP.ShowDialog();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void CONSULTAR_PRODUCTO_Load(object sender, EventArgs e)
        {

        }
    }
}
