﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using final.CAPA_DATOS;
namespace CAPA_PRESENTACION
{
    public partial class CREAR_FACTURA : Form
    {
        DataGridViewRow[] old;
        public CREAR_FACTURA()
        {
            InitializeComponent();
            ConsultasSQL sql = new ConsultasSQL();
            DataTable datos = sql.ListarProductosFactura();
            dataGridView1.DataSource = datos;

            foreach (DataGridViewColumn dc in dataGridView1.Columns)
            {
                if (dc.Index.Equals(0) || dc.Index.Equals(1))
                {
                    dc.ReadOnly = false;
                }
                else
                {
                    dc.ReadOnly = true;
                }
            }
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

        }

       
        private void regreso_Click(object sender, EventArgs e)
        {
            this.Hide();
            menu_principal hr = new menu_principal();
            hr.ShowDialog();
        }

        private void INFORMES_Load(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            labelTot.Text = "";
            labelTot.Visible = false;
            //obtengo los productos que se hallan seleccionado
            var filasChecadas = from DataGridViewRow r in dataGridView1.Rows
                              where Convert.ToBoolean(r.Cells[0].Value) == true
                              select r;
            string productos = "";
            int total = 0;
            foreach (var fila in filasChecadas)
            {
                int cantidadseleccionada = Int32.Parse(fila.Cells["cantidadcomprar"].Value.ToString());
                int cantidadExistente = Int32.Parse(fila.Cells["cantidad"].Value.ToString());
                if (cantidadseleccionada > cantidadExistente)
                {
                    MessageBox.Show("La cantidad seleccionada supera al stock en algunos productos");
                    return;
                }
                int precio = Int32.Parse(fila.Cells["cantidadcomprar"].Value.ToString()) * Int32.Parse(fila.Cells["precio"].Value.ToString());
                productos += fila.Cells["nombre"].Value.ToString()+"X"+ fila.Cells["cantidadcomprar"].Value.ToString()+"="+precio.ToString()+"\r\n";
                total += precio;
            }
            productos += "\r\nTOTAL:" + total.ToString();
            labelTot.Text = productos;
            labelTot.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //obtengo los productos que se hallan seleccionado
            var checkedRows = from DataGridViewRow r in dataGridView1.Rows
                              where Convert.ToBoolean(r.Cells[0].Value) == true
                              select r;

            int total = 0;
            List<string> ids = new List<string>();
            foreach (var row in checkedRows)
            {
                int precio = Int32.Parse(row.Cells["cantidadcomprar"].Value.ToString()) * Int32.Parse(row.Cells["precio"].Value.ToString());
                ids.Add(row.Cells["codigo"].Value.ToString()+","+ row.Cells["cantidadcomprar"].Value.ToString());
                total += precio;
            }

            ConsultasSQL sql = new ConsultasSQL();
            bool resultado = sql.insertarVenta(ids,total.ToString());
            if (resultado)
            {
                MessageBox.Show("FACTURA GUARDADA");
                this.Hide();
                FACTURAS pt = new FACTURAS();
                pt.ShowDialog();
            }
        }
    }
}
