﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using final.CAPA_DATOS;

namespace CAPA_PRESENTACION
{
    public partial class LOGIN : Form
    {
        string usuario, contraseña;
        public LOGIN()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            //obtienen los valores ingresados por el usuario
            usuario = TextBox1.Text;
            contraseña = TextBox2.Text;
            //Llamar al archivo donde estan las consultas
            ConsultasSQL sql = new ConsultasSQL();
            //consulta para verificar si el usuario y el password son correctos
            DataTable resultado = sql.consultaLogin(usuario,contraseña);
            //valida si la consulta trajo algo o no trajo nada
            if (resultado.Rows.Count > 0)
            {
                //almacena temporalmente el usuario y el rol
                string rol = "";
                string usuario = "";
                foreach (DataRow row in resultado.Rows)
                {
                    rol = TextBox1.Text = row["id_rol"].ToString();
                    usuario = TextBox1.Text = row["id"].ToString();

                }
                MessageBox.Show("BIENVENID0");
                // mando el rol y el usuario que se logueo
                SPLASH pr = new SPLASH(rol, usuario);
                pr.ShowDialog();
    
            }
            else MessageBox.Show("DATOS INCORRECTOS");
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            Application.ExitThread();
        }

        private void Label1_Click(object sender, EventArgs e)
        {
        
        }

        private void LOGIN_Load(object sender, EventArgs e)
        {

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
