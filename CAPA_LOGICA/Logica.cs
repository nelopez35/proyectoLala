﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using final.CAPA_DATOS;

namespace final.CAPA_LOGICA
{
    class Logica
    {

        public string[] obtenerFunciones(string rol)
        {
            ConsultasSQL sql = new ConsultasSQL();
            //RECIBE EL TEXTO DEL ROL EJE e,pro,p
            string rolTexto = sql.obtenerFunciones(rol);
            var funciones = new string[] { };
            if (rolTexto == "all")
            {
                funciones[0] = "all";
            }
            else
            {
                // convierte el texto del rol (f,e,pro) en un array asi [f, e, pro]
                funciones = rolTexto.Split(',');
            }
            return funciones;
        }
    }
}
